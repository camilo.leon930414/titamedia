
/* menu */

function menu() {
    var items = document.getElementsByClassName("item");
    console.log(items)
    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        item.classList.toggle("active");
    }
}

/* funciones de mostrar cpor lista o por bloque */
function list(){
    var grid = document.getElementsByClassName("grid")[0];
    if(grid!= undefined){
        grid.className='gridlist';
    }
    this.findImg();
}

function block() {
    var grid = document.getElementsByClassName("gridlist")[0];
    if(grid!= undefined){
        grid.className='grid';
    }
    this.findImg();
}

function findImg() {
    var img = document.getElementsByTagName("img");
    for (let i = 0; i < img.length; i++) {
        const element = img[i];
        element.classList.toggle("img_list");
    }
}