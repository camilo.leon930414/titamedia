
var imagenes = [];

/* imagenes */

imagenes=[
    {
        "nombre":"imagen #1",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #2",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #3",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #4",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #5",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #6",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #7",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #8",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #9",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #10",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #11",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #12",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #13",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #14",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #15",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #16",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #17",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #18",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #19",
        "alt":"galeria",
        "url":"foto"
    },
    {
        "nombre":"imagen #20",
        "alt":"galeria",
        "url":"foto"
    }
]

function load() {
    const grid = document.querySelector(".grid");
    /* usar un contador por el formato que hice con los nombres de las fotos */
    var count_img = 0;
    imagenes.forEach(item => {
        count_img++;
        const div = document.createElement("div");
        const a = document.createElement("a");
        const img = document.createElement("img");
        /* agregar etiqueta hipervinculo a cada imagen */
        a.href = "../img/"+item['url']+count_img+'.jfif';
        a.target="_blank";
        /* agrego la clase del box grid */
        div.classList.add("box_grid");
        /* creo el elemento img para agregarlo al div */
        img.src = "../img/"+item['url']+count_img+'.jfif';
        img.alt =  item['alt'];        
        a.appendChild(img);
        div.appendChild(a);
        /* añado el div cin la imagen al grid */
        grid.appendChild(div);
   }); 
}